<?

/*
сделано только 2 рабочих обработчика:
обработчик отменяющий сохранение элемента инфоблока если не задан детальный текст
обработчик который добавляет добавляет количество символов детального текста в свойство
*/

// обработчик отменяющий сохранение элемента инфоблока если не задан детальный текст
AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", Array("BeforeDetailText", "OnBeforeIBlockElementUpdateHandler"));

//обработчик который добавляет добавляет количество символов детального текста в свойство
AddEventHandler("iblock", "OnAfterIBlockElementUpdate", Array("AfterDetailText", "OnAfterIBlockElementUpdateHandler"));

//обработчик события добавления элемента инфоблока, который будет отсылать уведомление администратору с информацией о созданном элементе
AddEventHandler("iblock", "OnAfterIBlockElementAdd", Array("SendNotify", "OnAfterIBlockElementAddHandler"));

//обработчик на регистрацию добавляющий счетчик зарегистрированных пользователей за последние сутки
AddEventHandler("main", "OnAfterUserAdd", Array("NewUser", "OnAfterUserAddHandler"));

//обработчик который при сохранение элемента инфоблока автоматически заполняет поле сортировки в зависимости 
//от всех элементов инфоблока в алфавитном порядке
AddEventHandler("iblock", "OnAfterIBlockElementUpdate", Array("Sort", "OnAfterIBlockElementUpdateHandler"));

class Sort
{
    function OnAfterIBlockElementUpdateHandler(&$arFields)
    {
        
        $news = $arFields['IBLOCK_ID'];
		
		$arSelect = array("ID",  "IBLOCK_ID",  "NAME",  "SORT");
		$arFilter = Array("IBLOCK_ID"=>IntVal($news), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
		
		$res = CIBlockElement::GetList(array("NAME"=>"ASC"), $arFilter,  false, false,  $arSelect);
				
		while($ar_fields = $res->GetNext())
		{
		// dump($ar_fields);
		/* for(var i = 0, i < ){
			$IBLOCK_ID = $ar_fields["IBLOCK_ID"];
			$ELEMENT_ID = $ar_fields["ID"];
			$PROPERTY_VALUE = ;
			$PROPERTY_CODE = "SORT"; 
		
		CIBlockElement::SetPropertyValues($ELEMENT_ID, $IBLOCK_ID, $PROPERTY_VALUE, $PROPERTY_CODE);
		}*/
		}	
		   
    }
}


class NewUser
{	
    function OnAfterUserAddHandler(&$arFields)
    {	
        if($arFields["ID"]>0){
			
				$db_props = CIBlockElement::GetProperty($arFields['IBLOCK_ID'], $arFields['ID'], array("sort" => "asc"), Array("CODE"=>"NEW_USERS"));
		if($ar_props = $db_props->Fetch())
			
			dump($ar_props);		
			
		}           
    }
}

class SendNotify
{
	
    function OnAfterIBlockElementAddHandler(&$arFields)
    {
		//add post pattern
		$arr["ACTIVE"]      = "Y";
		$arr["EVENT_NAME"]  = "ELEM_ADD";
		$arr["LID"]         = "s1";
		$arr["EMAIL_FROM"]  = "admin@site.ru";
		$arr["EMAIL_TO"]    = "admin@site.ru";
		$arr["BCC"]         = "";
		$arr["SUBJECT"]     = "Добавлен новый элемент #ID#";
		$arr["BODY_TYPE"]   = "text";
		$arr["MESSAGE"]     = "Элемент #ID# Добавлен.";
		$obTemplate = new CEventMessage;
		$obTemplate->Add($arr);
	
	dump($obTemplate);
	
		$arFields = array(
			"ID"          => 82,
			"CONTRACT_ID" => 1,
			"TYPE_SID"    => "LEFT"
			);
		CEvent::Send("ELEM_ADD", "s1", $arFields);
		
    }
}

class AfterDetailText
{
    function OnAfterIBlockElementUpdateHandler(&$arFields)
    {  
        if($arFields["DETAIL_TEXT"])   {
		
		$toWright = strlen($arFields["DETAIL_TEXT"]);
		$ELEMENT_ID = $arFields['ID']; 
		$PROPERTY_CODE = "DET_COUNT"; 
		$PROPERTY_VALUE = $toWright;  

		$dbr = CIBlockElement::GetList(array(), array("=ID"=>$ELEMENT_ID), false, false, array("ID", "IBLOCK_ID"));
			if ($dbr_arr = $dbr->Fetch())
			{
			  $IBLOCK_ID = $dbr_arr["IBLOCK_ID"];
			  CIBlockElement::SetPropertyValues($ELEMENT_ID, $IBLOCK_ID, $PROPERTY_VALUE, $PROPERTY_CODE);
			}
		}
    }
}

class BeforeDetailText
{
    function OnBeforeIBlockElementUpdateHandler(&$arFields)
    {		
	
		if(strlen($arFields["DETAIL_TEXT"])<=0)
        {
            global $APPLICATION;
            $APPLICATION->throwException("Детальное описание пустое.");			
            return false;
        }
    }
}

?>